<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>Modify - Book</title>
</head>
<body>
	<form action="/biblioteca/book/modify" method="post">
		<h2>Modify book:</h2>
		<p>
			<label>Book ID:</label> <input type="text" name="id" required />
		</p>
		<br>
		<p>
			<label>Title:</label> <input type="text" name="title" required />
		</p>
		<p>
			<label>Editorial:</label> <input type="text" name="editorial" />
		</p>
		<p>
			<label>Author:</label> <input type="text" name="author" required />
		</p>
		<p>
			<label>Genre:</label> <input type="text" name="genre" required />
		</p>
		<p>
			<label>Author country:</label> <input type="text" name="country" />
		</p>
		<p>
			<label>Number of pages:</label> <input type="text" name="pages"
				required />
		</p>
		<p>
			<label>Published year:</label> <input type="text" name="year" />
		</p>
		<p>
			<label>Amount of copies:</label> <input type="text" name="amount" />
		</p>


		<p>
			<input type="submit" value="Submit">
		</p>
	</form>
</body>
</html>