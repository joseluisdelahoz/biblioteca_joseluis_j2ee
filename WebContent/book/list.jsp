<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List Books</title>
</head>
<body>
	<h1>List Books</h1>
	<table>
		<c:forEach items="${book_list}" var="book">
			<tr>
				<td>${book.getId()}</td>
				<td>${book.getTitle()}</td>
				<td>${book.getEditorial()}</td>
				<td>${book.getAuthor()}</td>
				<td>${book.getGenre()}</td>
				<td>${book.getAuthorCountry()}</td>
				<td>${book.getNumberOfPages()}</td>
				<td>${book.getEditionYear()}</td>
				<td>${book.getAmountOfCopies()}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>