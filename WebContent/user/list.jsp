<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List Users</title>
</head>
<body>
	<h1>List Users</h1>
	<table>
		<c:forEach items="${list_users}" var="user">
			<tr>
				<td>${user.getId()}</td>
				<td>${user.getName()}</td>
				<td>${user.getFirstSurname()}</td>
				<td>${user.getSecondSurname()}</td>
				<td>${user.getDni()}</td>
				<td>${user.getDomicile()}</td>
				<td>${user.getTown()}</td>
				<td>${user.getProvince()}</td>
				<td>${user.getBirth()}</td>
				<td>${user.getPenalties()}</td>
			</tr>
		</c:forEach>
	</table>
	<h2>Users without penalties</h2>
	<table>
		<c:forEach items="${list_users_without_penalties}" var="user">
			<tr>
				<td>${user.getId()}</td>
				<td>${user.getName()}</td>
				<td>${user.getFirstSurname()}</td>
				<td>${user.getSecondSurname()}</td>
				<td>${user.getDni()}</td>
				<td>${user.getDomicile()}</td>
				<td>${user.getTown()}</td>
				<td>${user.getProvince()}</td>
				<td>${user.getBirth()}</td>
				<td>${user.getPenalties()}</td>
			</tr>
		</c:forEach>
	</table>
	<h2>Users with penalties:</h2>
	<table>
		<c:forEach items="${list_users_with_penalties}" var="user">
			<tr>
				<td>${user.getId()}</td>
				<td>${user.getName()}</td>
				<td>${user.getFirstSurname()}</td>
				<td>${user.getSecondSurname()}</td>
				<td>${user.getDni()}</td>
				<td>${user.getDomicile()}</td>
				<td>${user.getTown()}</td>
				<td>${user.getProvince()}</td>
				<td>${user.getBirth()}</td>
				<td>${user.getPenalties()}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>