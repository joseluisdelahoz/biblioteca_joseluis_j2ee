<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modify - User</title>
</head>
<body>
	<form action="/biblioteca/user/modify" method="post">
		<h2>Modify book:</h2>
		<p>
			<label>ID:</label> <input type="text" name="id" required />
		</p><br>
		<p>
			<label>Name:</label> <input type="text" name="name" required />
		</p>
		<p>
			<label>First surname:</label> <input type="text" name="first-surname" />
		</p>
		<p>
			<label>Second surname:</label> <input type="text"
				name="second-surname" required />
		</p>
		<p>
			<label>DNI:</label> <input type="text" name="dni" required />
		</p>
		<p>
			<label>Domicile:</label> <input type="text" name="domicile" />
		</p>
		<p>
			<label>Town:</label> <input type="text" name="town" required />
		</p>
		<p>
			<label>Province:</label> <input type="text" name="province" />
		</p>
		<p>
			<label>Birth (day/month/year):</label> <input type="text"
				name="birth" />
		</p>
		<p>
			<label>Penalties:</label> <input type="text" name="penalties" />
		</p>
		<p>
			<input type="submit" value="Submit">
		</p>
	</form>

</body>
</html>