<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="/biblioteca/user/add" method="post">
		<h2>Add book:</h2>
		<p>
			<label>Name:</label> <input type="text" name="name" required />
		</p>
		<p>
			<label>First surname:</label> <input type="text" name="first-surname" />
		</p>
		<p>
			<label>Second surname:</label> <input type="text" name="second-surname" required />
		</p>
		<p>
			<label>DNI:</label> <input type="text" name="dni" required />
		</p>
		<p>
			<label>Domicile:</label> <input type="text" name="domicile" />
		</p>
		<p>
			<label>Town:</label> <input type="text" name="town"
				required />
		</p>
		<p>
			<label>Province:</label> <input type="text" name="province" />
		</p>
		<p>
			<label>Birth (day/month/year):</label> <input type="text" name="birth" />
		</p>

		<p>
			<input type="submit" value="Submit">
		</p>
	</form>

</body>
</html>