<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Remove - User</title>
</head>
<body>
	<form action="/biblioteca/user/remove" method="post">
		<h2>Remove user:</h2>
		<p>
			<label>User ID:</label> <input type="text" name="id" required />
		</p>
		<p>
			<input type="submit" value="Submit">
		</p>
	</form>
</body>
</html>