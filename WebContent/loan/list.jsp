<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>List Loans</h1>
	<table>
		<c:forEach items="${loan_list}" var="loan">
			<tr>
				<td>${loan}</td>
			</tr>
		</c:forEach>
	</table>
	<h1>List Returned Loans</h1>
	<table>
		<c:forEach items="${loan_returned_list}" var="loan">
			<tr>
				<td>${loan}</td>
			</tr>
		</c:forEach>
	</table>
	<h1>List Not Returned Loans</h1>
	<table>
		<c:forEach items="${loan_not_returned_list}" var="loan">
			<tr>
				<td>${loan}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
