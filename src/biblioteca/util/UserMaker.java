package biblioteca.util;

import java.sql.Date;

import biblioteca.model.User;

public class UserMaker {
	
	private UserMaker() {
		throw new IllegalStateException("Utility class");
	}

	public static User newInstance(String name, String firstSurname, String secondSurname, String dni, String domicile,
			String town, String province, Date birth, int penalties) {
		User user = new User();
		user.setName(name);
		user.setFirstSurname(firstSurname);
		user.setSecondSurname(secondSurname);
		user.setDni(dni);
		user.setDomicile(domicile);
		user.setTown(town);
		user.setProvince(province);
		user.setBirth(birth);
		user.setPenalties(penalties);
		return user;
	}

	public static User newInstance(int id, String name, String firstSurname, String secondSurname, String dni, String domicile,
			String town, String province, Date birth, int penalties) {
		User user = new User();
		user.setId(id);
		user.setName(name);
		user.setFirstSurname(firstSurname);
		user.setSecondSurname(secondSurname);
		user.setDni(dni);
		user.setDomicile(domicile);
		user.setTown(town);
		user.setProvince(province);
		user.setBirth(birth);
		user.setPenalties(penalties);
		return user;
	}

	public static User newInstance(String name, String firstSurname, String secondSurname, String dni, String domicile,
			String town, String province, Date birth) {
		User user = new User();
		user.setName(name);
		user.setFirstSurname(firstSurname);
		user.setSecondSurname(secondSurname);
		user.setDni(dni);
		user.setDomicile(domicile);
		user.setTown(town);
		user.setProvince(province);
		user.setBirth(birth);
		return user;
	}
}
