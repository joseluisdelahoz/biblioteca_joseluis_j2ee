package biblioteca.util;

import biblioteca.model.Book;

public class BookMaker {

	private BookMaker() {
		throw new IllegalStateException("Utility class");
	}

	public static Book newInstance(int id, String title, String editorial, String author, String genre,
			String authorCountry, int numberOfPages, String editionYear, int amountOfCopies) {
		Book book = new Book();
		book.setId(id);
		book.setTitle(title);
		book.setEditorial(editorial);
		book.setAuthor(author);
		book.setGenre(genre);
		book.setAuthorCountry(authorCountry);
		book.setNumberOfPages(numberOfPages);
		book.setEditionYear(editionYear);
		book.setAmountOfCopies(amountOfCopies);
		return book;
	}

	public static Book newInstance(String title, String editorial, String author, String genre,
			String authorCountry, int numberOfPages, String editionYear, int amountOfCopies) {
		Book book = new Book();
		book.setTitle(title);
		book.setEditorial(editorial);
		book.setAuthor(author);
		book.setGenre(genre);
		book.setAuthorCountry(authorCountry);
		book.setNumberOfPages(numberOfPages);
		book.setEditionYear(editionYear);
		book.setAmountOfCopies(amountOfCopies);
		return book;
	}
}
