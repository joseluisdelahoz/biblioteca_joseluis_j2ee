package biblioteca.util;

import java.sql.Date;

import biblioteca.model.Loan;

public class LoanMaker {
	
	private LoanMaker() {
		throw new IllegalStateException("Utility class");
	}

	public static Loan newInstance(int bookId, int userId, Date borrow, Date borrowDeadline) {
		Loan loan = new Loan();
		loan.setBookId(bookId);
		loan.setUserId(userId);
		loan.setBorrowing(borrow);
		loan.setDevolutionDeadline(borrowDeadline);
		return loan;
	}

}
