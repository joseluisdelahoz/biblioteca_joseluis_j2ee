package biblioteca.control.user;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.dao.UserDAO;
import biblioteca.model.User;
import biblioteca.util.UserMaker;

/**
 * Servlet implementation class AddUserController
 */
@WebServlet("/user/add")
public class AddUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddUserController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET: user add page");
		RequestDispatcher dispatcher = request.getRequestDispatcher("add.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			System.out.println("POST: add book");
			String name = request.getParameter("name");
			String firstSurname = request.getParameter("first-surname");
			String secondSurname = request.getParameter("second-surname");
			String dni = request.getParameter("dni");
			String domicile = request.getParameter("domicile");
			String town = request.getParameter("town");
			String province = request.getParameter("province");
			java.util.Date birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("birth"));
			Date birth = new Date(birthdate.getTime());
			User user = UserMaker.newInstance(name, firstSurname, secondSurname, dni, domicile, town, province, birth);
			int status = UserDAO.add(user);
			System.out.println(status);
			response.sendRedirect("/biblioteca/user/");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
