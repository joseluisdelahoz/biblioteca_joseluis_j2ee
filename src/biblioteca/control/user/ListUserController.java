package biblioteca.control.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.dao.UserDAO;

/**
 * Servlet implementation class ListUserController
 */
@WebServlet("/user/list")
public class ListUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListUserController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET: list user");
		request.setAttribute("list_users", UserDAO.getUsers());
		request.setAttribute("list_users_without_penalties", UserDAO.getUsersWithoutPenalties());
		request.setAttribute("list_users_with_penalties", UserDAO.getUsersWithtPenalties());
		request.getRequestDispatcher("list.jsp").forward(request, response);
	}

}
