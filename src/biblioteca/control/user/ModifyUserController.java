package biblioteca.control.user;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.dao.UserDAO;
import biblioteca.model.User;
import biblioteca.util.UserMaker;

/**
 * Servlet implementation class ModifyUserController
 */
@WebServlet("/user/modify")
public class ModifyUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModifyUserController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET: modify user page");
		request.getRequestDispatcher("modify.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			System.out.println("POST: modify book");
			String name = request.getParameter("name");
			String firstSurname = request.getParameter("first-surname");
			String secondSurname = request.getParameter("second-surname");
			String dni = request.getParameter("dni");
			String domicile = request.getParameter("domicile");
			String town = request.getParameter("town");
			String province = request.getParameter("province");
			java.util.Date birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("birth"));
			Date birth = new Date(birthdate.getTime());
			int penalties = Integer.parseInt(request.getParameter("penalties"));
			int id = Integer.parseInt(request.getParameter("id"));
			User user = UserMaker.newInstance(name, firstSurname, secondSurname, dni, domicile, town, province, birth, penalties);
			user.setId(id);
			int status = UserDAO.modify(user);
			System.out.println(status);
			response.sendRedirect("/biblioteca/user/");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
