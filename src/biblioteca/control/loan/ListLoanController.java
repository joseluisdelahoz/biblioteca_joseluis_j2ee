package biblioteca.control.loan;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.dao.LoanDAO;

/**
 * Servlet implementation class ListLoanController
 */
@WebServlet("/loan/list")
public class ListLoanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListLoanController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET: list loans");
		request.setAttribute("loan_list", LoanDAO.getLoans());
		request.setAttribute("loan_not_returned_list", LoanDAO.getNotReturnedLoans());
		request.setAttribute("loan_returned_list", LoanDAO.getReturnedLoans());
		request.getRequestDispatcher("list.jsp").forward(request, response);
	}

}
