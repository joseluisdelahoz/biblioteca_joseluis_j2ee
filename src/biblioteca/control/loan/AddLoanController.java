package biblioteca.control.loan;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.dao.LoanDAO;
import biblioteca.model.Loan;
import biblioteca.util.LoanMaker;

/**
 * Servlet implementation class AddLoanController
 */
@WebServlet("/loan/add")
public class AddLoanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddLoanController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET: add loan page");
		RequestDispatcher dispatcher = request.getRequestDispatcher("add.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			System.out.println("POST: add loan");
			int bookId = Integer.parseInt(request.getParameter("book-id"));
			int userId = Integer.parseInt(request.getParameter("user-id"));
			java.util.Date borrowDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("borrow"));
			Date borrow = new Date(borrowDate.getTime());
			java.util.Date borrowDeadlineDate = new SimpleDateFormat("dd/MM/yyyy")
					.parse(request.getParameter("devolution-deadline"));
			Date devolutionDeadline = new Date(borrowDeadlineDate.getTime());
			Loan loan = LoanMaker.newInstance(bookId, userId, borrow, devolutionDeadline);
			int status = LoanDAO.add(loan);
			System.out.println(status);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		response.sendRedirect("/biblioteca/loan/");
	}

}
