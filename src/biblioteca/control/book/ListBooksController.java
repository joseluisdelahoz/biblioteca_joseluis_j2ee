package biblioteca.control.book;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.dao.BookDAO;

/**
 * Servlet implementation class ListBooksController
 */
@WebServlet("/book/list")
public class ListBooksController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public ListBooksController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET: list book");
		request.setAttribute("book_list", BookDAO.getBooks());
		request.getRequestDispatcher("list.jsp").forward(request, response);
	}
}
