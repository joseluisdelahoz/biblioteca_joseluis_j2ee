package biblioteca.control.book;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblioteca.model.Book;
import biblioteca.util.BookMaker;
import biblioteca.dao.BookDAO;

/**
 * Servlet implementation class ModifyBookController
 */
@WebServlet("/book/modify")
public class ModifyBookController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModifyBookController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("GET: modify book page");
		request.getRequestDispatcher("modify.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("POST: modify book");
		int id = Integer.parseInt(request.getParameter("id"));
		String title = request.getParameter("title");
		String editorial = request.getParameter("editorial");
		String author = request.getParameter("author");
		String genre = request.getParameter("genre");
		String country = request.getParameter("country");
		int pages = Integer.parseInt(request.getParameter("pages"));
		String year = request.getParameter("year");
		int copies = Integer.parseInt(request.getParameter("amount"));
		Book book = BookMaker.newInstance(id, title, editorial, author, genre, country, pages, year, copies);
		int status = BookDAO.modify(book);
		System.out.println(status);
		response.sendRedirect("/biblioteca/book/");
	}

}
