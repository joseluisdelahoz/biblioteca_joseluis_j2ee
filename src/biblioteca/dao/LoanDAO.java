package biblioteca.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biblioteca.model.Book;
import biblioteca.model.Loan;
import biblioteca.util.Conexion;

public class LoanDAO {

	private LoanDAO() {
		throw new IllegalStateException("Utility class");
	}

	public static int add(Loan loan) {
		Conexion SQL = new Conexion();
		String sentencia = "INSERT INTO prestamos (librocodigo, usuariocodigo, fechaprestamo, fechamaximadevolucion) VALUES (?, ?, ?, ?)";

		int status = 0;
		try (Connection conn = SQL.conectarMySQL(); PreparedStatement ps = conn.prepareStatement(sentencia)) {
			ps.setInt(1, loan.getBookId());
			ps.setInt(2, loan.getUserId());
			ps.setDate(3, loan.getBorrowing());
			ps.setDate(4, loan.getDevolutionDeadline());
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static int remove(int id) {
		Conexion SQL = new Conexion();
		String sentencia = "DELETE FROM prestamos WHERE numeropedido = ?";
		int status = 0;

		try (Connection conn = SQL.conectarMySQL(); PreparedStatement ps = conn.prepareStatement(sentencia)) {
			ps.setInt(1, id);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static int giveBack(int id) {
		Conexion SQL = new Conexion();
		int status = 0;
		String sentencia = "UPDATE prestamos SET fechadevolucion=? WHERE numeropedido=?";
		try (Connection conn = SQL.conectarMySQL(); PreparedStatement ps = conn.prepareStatement(sentencia)) {
			ps.setDate(1, new Date(System.currentTimeMillis()));
			ps.setInt(2, id);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static List<String> getLoans() {
		List<String> loans = new ArrayList<>();
		String sentencia = "SELECT numeropedido, librocodigo, usuariocodigo, usuarios.nombre, libros.titulo\r\n"
				+ "FROM prestamos\r\n" + "JOIN usuarios ON prestamos.usuariocodigo=usuarios.codigousuario\r\n"
				+ "JOIN libros ON prestamos.librocodigo=libros.codigolibro\r\n" + ";";

		Conexion SQL = new Conexion();
		try (Connection conn = SQL.conectarMySQL(); PreparedStatement st = conn.prepareStatement(sentencia)) {

			ResultSet rs = st.executeQuery(sentencia);

			while (rs.next()) {
				int loanId = rs.getInt("numeropedido");
				int bookId = rs.getInt("librocodigo");
				int userId = rs.getInt("usuariocodigo");
				String name = rs.getString("nombre");
				String title = rs.getString("titulo");
				loans.add("Loan ID: " + loanId + " - Book ID: " + bookId + " - Title: " +  title + " - User ID: " + userId + " - Name: " + name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return loans;
	}

	public static List<String> getReturnedLoans() {
		List<String> loans = new ArrayList<>();
		String sentencia = "SELECT numeropedido, librocodigo, usuariocodigo, usuarios.nombre, libros.titulo\r\n"
				+ "FROM prestamos\r\n"
				+ "JOIN usuarios ON prestamos.usuariocodigo=usuarios.codigousuario AND prestamos.fechadevolucion IS NOT NULL\r\n"
				+ "JOIN libros ON prestamos.librocodigo=libros.codigolibro\r\n" + ";";

		Conexion SQL = new Conexion();
		try (Connection conn = SQL.conectarMySQL(); PreparedStatement st = conn.prepareStatement(sentencia)) {

			ResultSet rs = st.executeQuery(sentencia);

			while (rs.next()) {
				int loanId = rs.getInt("numeropedido");
				int bookId = rs.getInt("librocodigo");
				int userId = rs.getInt("usuariocodigo");
				String name = rs.getString("nombre");
				String title = rs.getString("titulo");
				loans.add("Loan ID: " + loanId + " - Book ID: " + bookId + " - Title: " +  title + " - User ID: " + userId + " - Name: " + name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return loans;
	}

	public static List<String> getNotReturnedLoans() {
		List<String> loans = new ArrayList<>();
		String sentencia = "SELECT numeropedido, librocodigo, usuariocodigo, usuarios.nombre, libros.titulo\r\n"
				+ "FROM prestamos\r\n"
				+ "JOIN usuarios ON prestamos.usuariocodigo=usuarios.codigousuario AND prestamos.fechadevolucion IS NULL\r\n"
				+ "JOIN libros ON prestamos.librocodigo=libros.codigolibro\r\n" + ";";

		Conexion SQL = new Conexion();
		try (Connection conn = SQL.conectarMySQL(); PreparedStatement st = conn.prepareStatement(sentencia)) {

			ResultSet rs = st.executeQuery(sentencia);

			while (rs.next()) {
				int loanId = rs.getInt("numeropedido");
				int bookId = rs.getInt("librocodigo");
				int userId = rs.getInt("usuariocodigo");
				String name = rs.getString("nombre");
				String title = rs.getString("titulo");
				loans.add("Loan ID: " + loanId + " - Book ID: " + bookId + " - Title: " +  title + " - User ID: " + userId + " - Name: " + name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return loans;
	}
}
