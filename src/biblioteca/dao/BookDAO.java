package biblioteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import biblioteca.model.Book;
import biblioteca.util.BookMaker;
import biblioteca.util.Conexion;

public class BookDAO {

	private BookDAO() {
		throw new IllegalStateException("Utility class");
	}

	public static int add(Book book) {
		Conexion SQL = new Conexion();
		Connection conn = SQL.conectarMySQL();

		int status = 0;
		try {
			String sentencia = "INSERT INTO libros (titulo, editorial, autor, genero, paisautor, numeropaginas, anoedicion, cantidad) values (?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sentencia);
			ps.setString(1, book.getTitle());
			ps.setString(2, book.getEditorial());
			ps.setString(3, book.getAuthor());
			ps.setString(4, book.getGenre());
			ps.setString(5, book.getAuthorCountry());
			ps.setInt(6, book.getNumberOfPages());
			ps.setString(7, book.getEditionYear());
			ps.setInt(8, book.getAmountOfCopies());
			status = ps.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static int modify(Book book) {
		Conexion SQL = new Conexion();
		int status = 0;

		try (Connection conn = SQL.conectarMySQL()) {
			String sentencia = "UPDATE libros SET titulo=?, editorial=?, autor=?, genero=?, paisautor=?, numeropaginas=?, anoedicion=?, cantidad=? WHERE codigolibro=?";
			PreparedStatement ps = conn.prepareStatement(sentencia);
			ps.setString(1, book.getTitle());
			ps.setString(2, book.getEditorial());
			ps.setString(3, book.getAuthor());
			ps.setString(4, book.getGenre());
			ps.setString(5, book.getAuthorCountry());
			ps.setInt(6, book.getNumberOfPages());
			ps.setString(7, book.getEditionYear());
			ps.setInt(8, book.getAmountOfCopies());
			ps.setInt(9, book.getId());
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static int remove(int id) {
		Conexion SQL = new Conexion();

		int status = 0;
		try (Connection conn = SQL.conectarMySQL()) {
			String sentencia = "DELETE FROM libros WHERE codigolibro = ?";
			PreparedStatement ps = conn.prepareStatement(sentencia);
			ps.setInt(1, id);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static ArrayList<Book> getBooks() {
		ArrayList<Book> books = new ArrayList<>();
		Conexion SQL = new Conexion();

		try (Connection conn = SQL.conectarMySQL()) {
			Statement st = conn.createStatement();
			String sql = ("SELECT * FROM libros");
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("codigolibro");
				String title = rs.getString("titulo");
				String editorial = rs.getString("editorial");
				String author = rs.getString("autor");
				String genre = rs.getString("genero");
				String authorCountry = rs.getString("paisautor");
				int numberOfPages = rs.getInt("numeropaginas");
				String editionYear = rs.getString("anoedicion");
				int amountOfCopies = rs.getInt("cantidad");
				Book book = BookMaker.newInstance(id, title, editorial, author, genre, authorCountry, numberOfPages,
						editionYear, amountOfCopies);
				books.add(book);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return books;
	}
}
