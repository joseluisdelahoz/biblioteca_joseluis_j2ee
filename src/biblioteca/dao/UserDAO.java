package biblioteca.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import biblioteca.model.User;
import biblioteca.util.Conexion;
import biblioteca.util.UserMaker;

public class UserDAO {

	private UserDAO() {
		throw new IllegalStateException("Utility class");
	}

	public static int add(User user) {
		Conexion SQL = new Conexion();
		String sentencia = "INSERT INTO usuarios (nombre, apellido1, apellido2, dni, domicilio, poblacion, provincia, fechanacimiento, penalizaciones) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		int status = 0;

		try (Connection conn = SQL.conectarMySQL(); PreparedStatement ps = conn.prepareStatement(sentencia)) {
			ps.setString(1, user.getName());
			ps.setString(2, user.getFirstSurname());
			ps.setString(3, user.getSecondSurname());
			ps.setString(4, user.getDni());
			ps.setString(5, user.getDomicile());
			ps.setString(6, user.getTown());
			ps.setString(7, user.getProvince());
			ps.setDate(8, user.getBirth());
			ps.setInt(9, user.getPenalties());
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static int remove(int id) {
		Conexion SQL = new Conexion();
		String sentencia = "DELETE FROM usuarios WHERE codigousuario = ?";
		int status = 0;

		try (Connection conn = SQL.conectarMySQL(); PreparedStatement ps = conn.prepareStatement(sentencia)) {
			ps.setInt(1, id);
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public static List<User> getUsers() {
		ArrayList<User> users = new ArrayList<>();
		Conexion SQL = new Conexion();
		String sql = ("SELECT * FROM usuarios");
		try (Connection conn = SQL.conectarMySQL(); Statement st = conn.createStatement()) {

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("codigousuario");
				String name = rs.getString("nombre");
				String firstSurname = rs.getString("apellido1");
				String secondSurname = rs.getString("apellido2");
				String dni = rs.getString("dni");
				String domicile = rs.getString("domicilio");
				String town = rs.getString("poblacion");
				String province = rs.getString("provincia");
				Date birth = rs.getDate("fechanacimiento");
				int penalties = rs.getInt("penalizaciones");
				User user = UserMaker.newInstance(id, name, firstSurname, secondSurname, dni, domicile, town, province,
						birth, penalties);
				users.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public static List<User> getUsersWithoutPenalties() {
		ArrayList<User> users = new ArrayList<>();
		Conexion SQL = new Conexion();
		String sql = ("SELECT * FROM usuarios WHERE penalizaciones <= 0");
		try (Connection conn = SQL.conectarMySQL(); Statement st = conn.createStatement()) {

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("codigousuario");
				String name = rs.getString("nombre");
				String firstSurname = rs.getString("apellido1");
				String secondSurname = rs.getString("apellido2");
				String dni = rs.getString("dni");
				String domicile = rs.getString("domicilio");
				String town = rs.getString("poblacion");
				String province = rs.getString("provincia");
				Date birth = rs.getDate("fechanacimiento");
				int penalties = rs.getInt("penalizaciones");
				User user = UserMaker.newInstance(id, name, firstSurname, secondSurname, dni, domicile, town, province,
						birth, penalties);
				users.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public static List<User> getUsersWithtPenalties() {
		ArrayList<User> users = new ArrayList<>();
		Conexion SQL = new Conexion();
		String sql = ("SELECT * FROM usuarios WHERE penalizaciones > 0");
		try (Connection conn = SQL.conectarMySQL(); Statement st = conn.createStatement()) {

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("codigousuario");
				String name = rs.getString("nombre");
				String firstSurname = rs.getString("apellido1");
				String secondSurname = rs.getString("apellido2");
				String dni = rs.getString("dni");
				String domicile = rs.getString("domicilio");
				String town = rs.getString("poblacion");
				String province = rs.getString("provincia");
				Date birth = rs.getDate("fechanacimiento");
				int penalties = rs.getInt("penalizaciones");
				User user = UserMaker.newInstance(id, name, firstSurname, secondSurname, dni, domicile, town, province,
						birth, penalties);
				users.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public static int modify(User user) {
		Conexion SQL = new Conexion();
		int status = 0;
		String sentencia = "UPDATE usuarios SET nombre=?, apellido1=?, apellido2=?, dni=?, domicilio=?, poblacion=?, provincia=?, fechanacimiento=?, penalizaciones=? WHERE codigousuario=?";
		try (Connection conn = SQL.conectarMySQL(); PreparedStatement ps = conn.prepareStatement(sentencia)) {
			ps.setString(1, user.getName());
			ps.setString(2, user.getFirstSurname());
			ps.setString(3, user.getSecondSurname());
			ps.setString(4, user.getDni());
			ps.setString(5, user.getDomicile());
			ps.setString(6, user.getTown());
			ps.setString(7, user.getProvince());
			ps.setDate(8, user.getBirth());
			ps.setInt(9, user.getPenalties());
			ps.setInt(10, user.getId());
			status = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

}
