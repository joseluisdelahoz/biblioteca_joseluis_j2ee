package biblioteca.model;

import java.sql.Date;
import java.util.Objects;

public class Loan {

	private int id;
	private int bookId;
	private int userId;
	private Date borrowing;
	private Date devolutionDeadline;
	private Date devolution;

	public Loan() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getBorrowing() {
		return borrowing;
	}

	public void setBorrowing(Date borrowing) {
		this.borrowing = borrowing;
	}

	public Date getDevolutionDeadline() {
		return devolutionDeadline;
	}

	public void setDevolutionDeadline(Date devolutionDeadline) {
		this.devolutionDeadline = devolutionDeadline;
	}

	public Date getDevolution() {
		return devolution;
	}

	public void setDevolution(Date devolution) {
		this.devolution = devolution;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bookId, borrowing, devolution, devolutionDeadline, id, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loan other = (Loan) obj;
		return bookId == other.bookId && Objects.equals(borrowing, other.borrowing)
				&& Objects.equals(devolution, other.devolution)
				&& Objects.equals(devolutionDeadline, other.devolutionDeadline) && id == other.id
				&& userId == other.userId;
	}

}
