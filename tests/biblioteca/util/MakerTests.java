package biblioteca.util;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;

import biblioteca.model.Loan;
import biblioteca.model.User;

class MakerTests {

	@Test
	void testLoanMaker() {
		long milis = System.currentTimeMillis();
		Loan loan = LoanMaker.newInstance(50, 5, new Date(milis), new Date(milis));
		Loan loan2 = new Loan();
		loan2.setBookId(50);
		loan2.setUserId(5);
		loan2.setBorrowing(new Date(milis));
		loan2.setDevolutionDeadline(new Date(milis));
		boolean comp = loan.equals(loan2);
		assertEquals(comp, true);
	}
	
	@Test
	void testUserMaker() {
		long milis = System.currentTimeMillis();
		Loan loan = LoanMaker.newInstance(50, 5, new Date(milis), new Date(milis));
		Loan loan2 = new Loan();
		loan2.setBookId(50);
		loan2.setUserId(5);
		loan2.setBorrowing(new Date(milis));
		loan2.setDevolutionDeadline(new Date(milis));
		boolean comp = loan.equals(loan2);
		assertEquals(comp, true);
	}
	
	@Test
	void testBookMaker() {
		long milis = System.currentTimeMillis();
		Loan loan = LoanMaker.newInstance(50, 5, new Date(milis), new Date(milis));
		Loan loan2 = new Loan();
		loan2.setBookId(50);
		loan2.setUserId(5);
		loan2.setBorrowing(new Date(milis));
		loan2.setDevolutionDeadline(new Date(milis));
		boolean comp = loan.equals(loan2);
		assertEquals(comp, true);
	}

}
